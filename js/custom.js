

$(document).ready(function () {
    $(".owl-carousel").owlCarousel(
        {
            loop: true,
            nav: true,
            navText: ["<span class='material-icons'>west</span>", "<span class='material-icons'>east</span>"],
            autoplay:true,
            responsiveClass:true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 2,
                    loop: true,
                },
                // breakpoint from 480 up
                480: {
                    items: 2,  
                    loop: true,              
                },
                // breakpoint from 768 up
                768: {
                    items: 4, 
                    loop: true,                 
                },
                // breakpoint from 768 up
                1024: {
                    items: 6, 
                    loop: true,                
                }
            },
           
        }
    );
   
});



